import React from 'react';
import Header from '../components/Header';
import Breadcrumb from '../components/Breadcrumb';
import TicketList from '../components/TicketList';
import Filters from '../components/Filters';

const HomePage = () => {

  return (
    <>
      <Header title="deônibus"/>
      <Breadcrumb pages={['Início', 'Página anterior', 'Página atual']} />
      <div className="row content-container">
        <TicketList />
        <Filters />
      </div>
    </>
  );
}

export default HomePage;
