/* eslint-disable import/no-named-as-default */
import React from "react";
import { Route, Switch } from "react-router-dom";
import HomePage from "../pages/HomePage";
import PropTypes from "prop-types";
import { hot } from "react-hot-loader";

class App extends React.Component {
  render() {
    return (
      <>
        <Switch>
          <Route exact path="/" component={HomePage} />
          {/* <Route component={NotFoundPage} /> */}
        </Switch>
      </>
    );
  }
}

App.propTypes = {
  children: PropTypes.element
};

export default hot(module)(App);
