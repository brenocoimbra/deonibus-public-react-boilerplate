// Set up your root reducer here...
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'
import mainReducer from './mainReducer';
import filters from './filtersReducer';

 const rootReducer = history => combineReducers({
  router: connectRouter(history),
  mainReducer,
  filters
});

 export default rootReducer;
