import { DEFAULT_FILTERS } from '../utils/constants';

export default {
  mainInfo: {
    tab: 0,
    favorites: []
  },
  filters: DEFAULT_FILTERS,
}
