import { DEFAULT_FILTERS } from '../utils/constants';

export default function filtersReducer(state = DEFAULT_FILTERS, action) {
  switch(action.type) {
    case 'FILTERS/UPDATE':
      return {
        ...state,
        [action.payload.accessKey]: action.payload.value
      }
    case 'FILTERS/CLEAR_CLASS':
      return {
        ...state,
        Convencional: false,
        Executivo: false,
        Leito: false,
        Semi: false,
        Double: false
      }
    case 'FILTERS/CLEAR':
      return {
        DEFAULT_FILTERS
      }
    default: return state;
  }
}
