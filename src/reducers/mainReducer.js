import initialState from './initialState';

export default function mainReducer(state = initialState.mainInfo, action) {
  switch(action.type) {
    case 'TABS/UPDATE':
      return {
        ...state,
        ...action.payload
      }
    case 'FAVORITES/ADD':
      return {
        ...state,
        favorites: [...state.favorites, action.payload.objectId]
      }
    case 'FAVORITES/REMOVE':
      return {
        ...state,
        favorites: state.favotires.filter(item => item !== action.payload.objectId)
      }
    default: return state;
  }
}
