import React, { useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import PropTypes from 'prop-types';
import { FiBookmark } from "react-icons/fi";
import { FcBookmark } from "react-icons/fc";
import { IconContext } from "react-icons";

import { getDuplicate } from '../../utils/functions';

import './style.scss';


const TicketItem = ({
  objectId,
  busClass,
  company,
  departure,
  arrival,
  origin,
  destination,
  price
}) => {
  const dispatch = useDispatch();
  const isFavorite = useSelector((state) => state.mainReducer ? state.mainReducer.favorites.find(item => item === objectId) : false);
  const parsedDeparture = useMemo(() => {
    const date = new Date(departure);
    return `${getDuplicate(date.getHours())}:${getDuplicate(date.getMinutes())}`
  }, [departure])
  const parsedArrival = useMemo(() => {
    const date = new Date(arrival);
    return `${getDuplicate(date.getHours())}:${getDuplicate(date.getMinutes())}`
  }, [arrival])

  const addFavorite = () => {
    dispatch({
      type: 'FAVORITES/ADD',
      payload: {
        objectId
      }
    })
  }

  const removeFavorite = () => {
    dispatch({
      type: 'FAVORITES/REMOVE',
      payload: {
        objectId
      }
    })
  }

  return (
    <div className="ticket-container">
      <div className="ticket-inner row">
        <div className="col-1">
          <IconContext.Provider value={{ size: '20px' }}>
            <div>
              {
                isFavorite ? (
                  <FcBookmark onClick={removeFavorite} />
                ) : (
                  <FiBookmark onClick={addFavorite} />
                )
              }
              <img src={`assets/${company}.png`} className="brand-logo" />
              <span className="brand-name">
                Viação {company}
              </span>
            </div>
          </IconContext.Provider>
        </div>
        <div className="col-2">
          <div className="row m-lft-10 time-container">
            <div className="col-1">
              <p className="initial-time">
                {parsedDeparture}
              </p>
            </div>
            <div className="col-3">
              <div className="time-divider">
              </div>
            </div>
            <div className="col-1">
              <p className="initial-time finish-time">
                {parsedArrival}
              </p>
            </div>
          </div>
          <div className="m-lft-10 m-top-10">
            <p className="from-to">
              de
              <span className="location-name">
                {' '}{origin}
              </span>
            </p>
            <p className="from-to">
              para
              <span className="location-name">
                {' '}{destination}
              </span>
            </p>
            <p className="bus-type">
              {busClass}
            </p>
          </div>

        </div>
        <div className="col-1">
          <p className="currency">
            R$
            <span className="price">
              {' '}{price.toFixed(2).replace('.', ',')}
            </span>
          </p>
          <p className="taxes">
            + taxas
          </p>
          <button className="buy-button">
            COMPRAR
          </button>
        </div>
      </div>
    </div>
  );
}

TicketItem.propTypes = {
  objectId: PropTypes.string,
  busClass: PropTypes.string,
  company: PropTypes.string,
  departure: PropTypes.string,
  arrival: PropTypes.string,
  origin: PropTypes.string,
  destination: PropTypes.string,
  price: PropTypes.number,
}


export default TicketItem;
