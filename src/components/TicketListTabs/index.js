import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import './style.scss'


const TicketListTabs = () => {
  const dispatch = useDispatch();
  const tab = useSelector((state) => state.mainReducer ? state.mainReducer.tab : 0);
  const tabs = ['mais cedo', 'mais tarde', 'menor preço', 'maior preço']

  const onClick = (tab) => {
    dispatch({
      type: 'TABS/UPDATE',
      payload: {
        tab
      }
    })
  }

  return (
    <div className="tabs-container">
      {
        tabs.map((item, index) => (
          <div key={item} className={`tab ${tab === index ? 'selected-tab' : ''}`} onClick={() => onClick(index)}>
            <p className="tab-title">
              {item}
            </p>
          </div>
        ))
      }
    </div>
  );
}

export default TicketListTabs;
