import React from 'react';

import FiltersHeader from '../FiltersHeader';
import TimeFilter from '../TimeFilter';
import ClassFilter from '../ClassFilter';
import BrandFilter from '../BrandFilter';
import FavoritesFilter from '../FavoritesFilter';

import './style.scss'

const Filters = () => {
  return (
    <div className="filters-container">
      <FiltersHeader />
      <TimeFilter />
      <ClassFilter />
      <BrandFilter />
      <FavoritesFilter />
    </div>
  );
}

export default Filters;
