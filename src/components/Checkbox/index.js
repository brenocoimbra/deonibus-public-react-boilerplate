import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import './style.scss'

const Checkbox = ({ title, accessKey }) => {
  const dispatch = useDispatch();
  const checked = useSelector((state) => state.filters ? state.filters[accessKey || title.split(' ')[0]] : false);

  const onClick = () => {
    dispatch({
      type: 'FILTERS/UPDATE',
      payload: {
        accessKey: accessKey || title.split(' ')[0],
        value: !checked
      }
    })
  }

  return (
    <div className="checkbox-container">
      <p className="checkbox-title">
        {title}
      </p>
      <div className="container" onClick={onClick}>
        <input type="checkbox" checked={checked} />
        <span className="checkmark"></span>
      </div>
    </div>
  );
}


Checkbox.propTypes = {
  title: PropTypes.string,
  accessKey: PropTypes.string,
}

export default Checkbox;
