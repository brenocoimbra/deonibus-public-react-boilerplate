import React from 'react';
import { GrLocation } from "react-icons/gr";
import { IconContext } from "react-icons";

import TicketListTabs from '../TicketListTabs';

import './style.scss'

const TicketListHeader = () => {

  return (
    <div className="ticket-header-title-container">
      <p className="ticket-header-title">
        IDA
      </p>
      <p className="ticket-header-date">
        seg, 27 de jan 2020
      </p>
      <IconContext.Provider value={{ className: 'location-icon', size: '23px' }}>
        <div className="location-divider">
          <div className="icon-col">
            <GrLocation />
          </div>
          <div className="text-col">
            <span className="location-text">
              Passagem de ônibus de São Paulo - SP para Rio de Janeiro - RJ
            </span>
            <p className="location-subtitle">
              Horários de ônibus e preços de passagens
            </p>
          </div>
        </div>
      </IconContext.Provider>
      <TicketListTabs selectedTab={0} />
    </div>
  );
}

export default TicketListHeader;
