import React from 'react';

import './style.scss'

const Header = () => {
  return (
    <>
      <div className="title-background">
        <img src="assets/logo.png" />
      </div>
      <div className="separator">
      </div>
    </>
  );
}

export default Header;
