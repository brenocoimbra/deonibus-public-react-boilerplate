import React from 'react';
import { useDispatch } from 'react-redux';
import { MdAirlineSeatReclineExtra } from 'react-icons/md';
import { IconContext } from "react-icons";

import Checkbox from '../Checkbox'

import './style.scss'

const ClassFilter = () => {
  const dispatch = useDispatch();

  const onClick = () => {
    dispatch({
      type: 'FILTERS/CLEAR_CLASS',
    })
  }

  return (
    <div className="class-filter-container">
      <div className="time-filter-inner">
        <IconContext.Provider value={{ size: '23px' }}>
          <div className="location-divider">
            <div className="icon-col pd-zero">
              <MdAirlineSeatReclineExtra />
            </div>
            <div className="text-col">
              <span className="location-text">
                Classes
              </span>
            </div>
          </div>
        </IconContext.Provider>
        <span className="time-filter-clean" onClick={onClick}>
          limpar tudo
        </span>
        <Checkbox title="Convencional" />
        <Checkbox title="Executivo" />
        <Checkbox title="Leito" />
        <Checkbox title="Semi Leito" />
        <Checkbox title="Double Check" />
      </div>
    </div>
  );
}

export default ClassFilter;
