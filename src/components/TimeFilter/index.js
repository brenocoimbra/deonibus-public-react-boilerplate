import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Range, getTrackBackground } from 'react-range';
import { AiOutlineClockCircle } from 'react-icons/ai';
import { IconContext } from "react-icons";

import { getDuplicate } from '../../utils/functions'

import './style.scss'

const TimeFilter = () => {
  const [values, setValues] = useState([0, 288]);
  const dispatch = useDispatch();
  const initialTime = useSelector((state) => state.filters ? state.filters.initialTime : '00:00');
  const finalTime = useSelector((state) => state.filters ? state.filters.finalTime : '24:00');

  useEffect(() => {
    dispatch({
      type: 'FILTERS/UPDATE',
      payload: {
        accessKey: 'initialTime',
        value: `${getDuplicate(Math.floor((values[0] * 5) / 60))}:${getDuplicate((values[0] * 5) % 60)}`
      }
    })
    dispatch({
      type: 'FILTERS/UPDATE',
      payload: {
        accessKey: 'finalTime',
        value: `${getDuplicate(Math.floor((values[1] * 5) / 60))}:${getDuplicate((values[1] * 5) % 60)}`
      }
    })
  }, [values]);

  useEffect(() => {
    setValues([
      (Number(initialTime.split(':')[0]) * 60 + Number(initialTime.split(':')[1])) / 5,
      (Number(finalTime.split(':')[0]) * 60 + Number(finalTime.split(':')[1])) / 5
    ])
  }, [initialTime, finalTime]);


  const background = getTrackBackground({
    values,
    colors: ['#C7C7C7', '#ED1941', '#C7C7C7'],
    min: 0,
    max: 288,
    rtl: true,
    direction: 'horizontal'
  })

  return (
    <div className="time-filter-container">
      <div className="time-filter-inner">
        <IconContext.Provider value={{ size: '23px' }}>
          <div className="location-divider">
            <div className="icon-col pd-zero">
              <AiOutlineClockCircle />
            </div>
            <div className="text-col">
              <span className="location-text">
                Horários de saída
              </span>
              <p className="time-subtitle">
                Rodoviária do Tietê - São Paulo - SP
              </p>
            </div>
          </div>
        </IconContext.Provider>
        <span className="time-filter-clean" onClick={() => setValues([0, 288])}>
          limpar tudo
        </span>
        <div className="row time-box-container">
          <div className="time-box">
            <p className="time-text">
              {initialTime}
            </p>
          </div>
          <div className="time-box box-margin">
            <p className="time-text">
              {finalTime}
            </p>
          </div>
        </div>
        <Range
          step={1}
          min={0}
          max={288}
          values={values}ya
          onChange={(values) => setValues(values)}
          renderTrack={({ props, children }) => (
            <div
              {...props}
              style={{
                ...props.style,
                background
              }}
              className="range-bar">
              {children}
            </div>
          )}
          renderThumb={({ props }) => (
            <div
              {...props}
              className="range-circle"
            />
          )}
        />
      </div>
    </div>
  );
}

export default TimeFilter;
