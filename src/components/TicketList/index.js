import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useSelector } from 'react-redux';

import { getDuplicate } from '../../utils/functions'

import TicketListHeader from '../TicketListHeader';
import TicketItem from '../TicketItem';

import './style.scss'

const TicketList = () => {
  const [tickets, setTickets] = useState([]);
  const [parsedTickets, setParsedTickets] = useState([]);
  const filters = useSelector((state) => state.filters ? state.filters : {});
  const tab = useSelector((state) => state.mainReducer ? state.mainReducer.tab : 0);
  const favorites = useSelector((state) => state.mainReducer ? state.mainReducer.favorites : [0]);

  useEffect(() => {
    axios({
      method: 'get',
      url: 'https://4jehkg0izj.execute-api.us-east-1.amazonaws.com/stage-v0/route',
      headers: {
        'X-Parse-Application-Id': 'LtD1wBDjTJB7CcuF3hNRNmvRI9CQpozYzN7jIxfA',
        'X-Parse-REST-API-Key': '1TMvuvt9n2qHCqdQ8qpLw7DX6wUQpq2zhq0OGTvp',
        'content-type': 'application/json'
      }
    }).then(res => {
      setTickets(res.data.results)
    }).catch(() => {
      alert('Aconteceu algo de errado!')
    })
  }, [])

  useEffect(() => {
    const list = tickets.filter(item => {
      let busClass = true;
      let company = true;
      let favorite = true;
      if (
        filters.Convencional ||
        filters.Executivo ||
        filters.Leito ||
        filters.Semi ||
        filters.Double
      ) {
        busClass = filters[item.BusClass.split(' ')[0]]
      }

      if (
        filters.Cometa ||
        filters.Catarinense ||
        filters['1001']
      ) {
        company = filters[item.Company.Name.split(' ')[0]]
      }

      if (filters.favoritesOnly) {
        favorite = Boolean(favorites.find(i => item.objectId === i));
      }
      const parsedDeparture = `${getDuplicate(new Date(item.DepartureDate.iso).getHours())}:${getDuplicate(new Date(item.DepartureDate.iso).getMinutes())}`
      // console.log(parsedDeparture, filters.initialTime, )

      const time = parsedDeparture > filters.initialTime && parsedDeparture < filters.finalTime

      return busClass && company && favorite && time;
    });

    setParsedTickets(list.sort((a, b) => {

      const parsedDepartureA = `${getDuplicate(new Date(a.DepartureDate.iso).getHours())}:${getDuplicate(new Date(a.DepartureDate.iso).getMinutes())}`
      const parsedDepartureB = `${getDuplicate(new Date(b.DepartureDate.iso).getHours())}:${getDuplicate(new Date(b.DepartureDate.iso).getMinutes())}`

      switch (tab) {
        case 0:
          if (parsedDepartureA === parsedDepartureB)
            return 0;
          return parsedDepartureA < parsedDepartureB ? -1 : 1
        case 1:
          if (parsedDepartureA === parsedDepartureB)
            return 0;
          return parsedDepartureA > parsedDepartureB ? -1 : 1
        case 2:
          if (a.Price === b.Price)
            return 0;
          return a.Price < b.Price ? -1 : 1
        case 3:
          if (a.Price === b.Price)
            return 0;
          return a.Price > b.Price ? -1 : 1
        default:
          return 0;
      }
    }))

  }, [filters, tab, favorites, tickets])

  return (
    <div className="ticket-background">
      <TicketListHeader />
      {
        parsedTickets.map((item) =>
          <TicketItem
            key={item.objectId}
            objectId={item.objectId}
            busClass={item.BusClass}
            company={item.Company.Name}
            departure={item.DepartureDate.iso}
            arrival={item.ArrivalDate.iso}
            price={item.Price}
            origin={item.Origin}
            destination={item.Destination}
          />
        )
      }
    </div>
  );
}

export default TicketList;
