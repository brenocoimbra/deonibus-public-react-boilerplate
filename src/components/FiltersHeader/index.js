import React from 'react';
import { useDispatch } from 'react-redux';

import './style.scss'

const FiltersHeader = () => {
  const dispatch = useDispatch();

  const onClick = () => {
    dispatch({
      type: 'FILTERS/CLEAR_CLASS',
    })
  }

  return (
    <div className="filters-header-container">
      <p className="filters-header-title">
        Filtro
      </p>
      <span className="filters-header-action" onClick={onClick}>
        limpar tudo
      </span>
    </div>
  );
}

export default FiltersHeader;
