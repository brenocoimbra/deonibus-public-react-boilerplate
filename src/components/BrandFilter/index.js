import React from 'react';
import { GrBus } from 'react-icons/gr';
import { IconContext } from "react-icons";

import Checkbox from '../Checkbox'

import './style.scss'

const BrandFilter = () => {

  return (
    <div className="brand-filter-container">
      <div className="time-filter-inner">
        <IconContext.Provider value={{ size: '23px' }}>
          <div className="location-divider">
            <div className="icon-col pd-zero">
              <GrBus />
            </div>
            <div className="text-col">
              <span className="location-text">
                Viações
              </span>
            </div>
          </div>
        </IconContext.Provider>
        <Checkbox title="Cometa" accessKey="Cometa" />
        <Checkbox title="Catarinense" accessKey="Catarinense" />
        <Checkbox title="1001" accessKey="1001" />
      </div>
    </div>
  );
}

export default BrandFilter;
