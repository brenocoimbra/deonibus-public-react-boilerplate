import React from 'react';
import PropTypes from 'prop-types';

import './style.scss'

const Header = ({ pages }) => {
  return (
    <div className="breadcrumb-background">
      {
        pages.map((item, index) => (
          <>
            {
              index !== pages.length - 1 ? (
                <span href="#" className="link">
                  {item}
                </span>
              ) : (
                <span className="current-page">
                  {item}
                </span>
              )
            }
            {
              index !== pages.length - 1 ? (
                <span className="arrow">
                  {'>'}
                </span>
              ) : null
            }
          </>
        ))
      }
    </div>
  );
}

Header.propTypes = {
  pages: PropTypes.arrayOf(PropTypes.string)
}

export default Header;
