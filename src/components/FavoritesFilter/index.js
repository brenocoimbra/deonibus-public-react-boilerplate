import React from 'react';
import { MdFavoriteBorder } from 'react-icons/md';
import { IconContext } from "react-icons";

import Checkbox from '../Checkbox'

import './style.scss'

const FavoritesFilter = () => {

  return (
    <div className="favorite-filter-container">
      <div className="time-filter-inner">
        <IconContext.Provider value={{ size: '23px' }}>
          <div className="location-divider">
            <div className="icon-col pd-zero">
              <MdFavoriteBorder />
            </div>
            <div className="text-col">
              <span className="location-text">
                Favoritos
              </span>
            </div>
          </div>
        </IconContext.Provider>
        <Checkbox title="apenas opções favoritas" accessKey="favoritesOnly" onClick={() => null} />
      </div>
    </div>
  );
}

export default FavoritesFilter;
