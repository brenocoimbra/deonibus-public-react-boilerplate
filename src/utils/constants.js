export const DEFAULT_FILTERS = {
  Convencional: false,
  Executivo: false,
  Leito: false,
  Semi: false,
  Double: false,
  Cometa: false,
  Catarinense: false,
  1001: false,
  favoritesOnly: false,
  initialTime: '00:00',
  finalTime: '24:00',
}
